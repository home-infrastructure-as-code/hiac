[[_TOC_]]

# Home network domain

## Introduction

The objective of this project is to help privacy-concerned individuals or small organizations to regain control of their data by deploying some services on top of infrastructure they own.

The approach is to define Docker cloud YAML files for each service, and a set of scripts to ease management.

The different files are structured in such a way that private configuration is stored in a directory separate from this repository. Although some configuration examples are provided, you have to write your own configuration files for each service.

## Prerequisites

This software requires a working installation of Docker Swarm.

## Operation

You can either use the `hiac.sh` script, which wraps some details, or directly use `docker stack`. Note that you will need to use `docker stack` anyways to run some commands such as `logs`.

Both options are described in the following sections.

### Using the `hiac.sh` script

The `hiac.sh` script covers the deployment of all services. This script must be run at the root of this repository.

#### Command reference

Run `./hiac.sh` to get a brief description of available options.

##### deploy

Performs some integrity checks and deploys the services to the Swarm.

###### Parameters

**`--container-config-dir`**: Path to the directory that contains the configuration files that will be used inside the Docker containers.

Check the documentation of every service for a detailed description of the expected files. Any other file in this directory is ignored. You can find examples of these files in the `container-config-examples` directory.

### Manually invoking `docker stack`

If you want more control over the process, you can always directly invoke the `docker stack` command. You'll have to make sure that you have the appropriate privileges, and that read access to the configuration files have the appropriate permissions.

You'll also need to declare some environment variables specific for each service (check service documentation).

## `Dockerfile`

Whenever available, oficial images are used. If no official image is available, then it is defined in https://gitlab.com/home-infrastructure-as-code/images.

## Services

### dnsmasq

#### container-config-dir

It must contain the following files:
* `dnsmasq.conf`: the dnsmasq configuration file. Refer to dnsmasq documentation for details on how to write this file.
* `hosts`: the `hosts` file that will be bound to `/etc/hosts` inside the container. You will need this file to define hostnames referring to static IP addresses (both IPv4 and IPv6).

#### Mandatory environment variables

##### `HOST_DNSMASQ_CONFIG_FILE`

Path to the dnsmasq configuration file on the host.

##### `HOST_ETC_HOSTS_FILE`

Path to the _hosts_ file on the host.

## Labels

This section describes Docker Swarm labels that are used across the different services. Refer to [1] for instructions on how to set Docker Swarm node labels.

* `hiac.service=<service id>`: Marks a node that can run the service with the given service identifier.

[1] https://docs.docker.com/engine/reference/commandline/node_update/

