#!/bin/bash

#    Dockerized home infrastructure as code helper script.
#    Copyright (C) 2021  Guillermo López Alejos
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

# For now only BASH is supported.
if [ "${BASH}x" = "x" ] ; then
	echo "Only BASH shell is supported." 1>&2
	exit 1
fi

set -o errexit
set -o nounset
set -o pipefail

HIAC_SCRIPT_VERSION=0.0.1
HIAC_SCRIPT_NAME="${0}"
HIAC_SCRIPT_ABS="$(realpath "${HIAC_SCRIPT_NAME}")"
HIAC_SCRIPT_DIR_ABS="$(dirname "${HIAC_SCRIPT_ABS}")"
HIAC_CLOUD_DIR_ABS="${HIAC_SCRIPT_DIR_ABS}/docker-cloud"
HIAC_SERVICES_DIR_ABS="${HIAC_CLOUD_DIR_ABS}/services"

# shellcheck source=docker-cloud/util/docker.sh
source "${HIAC_SCRIPT_DIR_ABS}/docker-cloud/util/docker.sh"

# shellcheck source=docker-cloud/util/license_and_conditions.sh
source "${HIAC_SCRIPT_DIR_ABS}/docker-cloud/util/license_and_conditions.sh"

###############################################################################
# 1. FUNCTIONS
###############################################################################

function print_help {
	cat <<- _TEXT_BLOCK
		HELP
		================================================================================
		<no help available>
	_TEXT_BLOCK
}

function print_usage_and_exit {
	cat 1>&2 <<- _TEXT_BLOCK
		${HIAC_SCRIPT_NAME} v${HIAC_SCRIPT_VERSION}

		Usage:

			${HIAC_SCRIPT_NAME} deploy --container-config-dir=<path>

			${HIAC_SCRIPT_NAME} <warranty | conditions>

		Options:
			--container-config-dir	Directory that contains the stack configuration files.

	_TEXT_BLOCK

	exit 1
}

#
# Function: for_each_service
# 
# Iterates over all the services calling the given function on each.
#
# Parameters:
#
# ${1} - Function to invoke for each service. It is passed the following parameters: ${1}: the service identifier.
#
function for_each_service {
	service_handler="${1}"

	for service_abs in "${HIAC_SERVICES_DIR_ABS}"/* ;
	do
		service_id="$(basename "${service_abs}")"
		"${service_handler}" "${service_id}"
	done
}

#
# Function: deploy_service
# 
# Prepares the 'docker swarm deploy' command line to deploy the service.
#
# Parameters:
#
# ${1} - Service identifier.
#
function deploy_service {
	service_id="${1}"
	service_dir="${HIAC_SERVICES_DIR_ABS}/${service_id}"

	# Source the default service environment.
	# shellcheck disable=SC1090
	source "${service_dir}/.env"

	# Source the custom service environment.
	custom_service_env="${container_config_dir}/${service_id}/.env"
	if [[ -f "${custom_service_env}" ]] ; then
		# shellcheck disable=SC1090
		source "${container_config_dir}/${service_id}/.env"
	fi

	DOCKER_SWARM_DEPLOY_ARGS+=("--compose-file" "${service_dir}/docker-cloud.yml")
}

###############################################################################
# 2. INPUT PARAMETER VALIDATION
###############################################################################
COMMAND=${1:-}
if [ -z "${COMMAND}" ] ; then
	echo "You must specify a command." 1>&2
	print_usage_and_exit
fi
shift

for param in "${@}" ; do
case "${param}" in
	--container-config-dir=*)
		container_config_dir="${param#*=}"
		shift
	;;
	*)
		echo "Unknown parameter ${param}." 1>&2
		print_help
		exit 1
	;;
esac
done

if [[ "${COMMAND}" = "deploy" ]] ; then
	if [[ "${container_config_dir:-}x" = "x" ]] ; then
		echo "Missing required parameter '--container-config-dir'." 1>&2
		print_usage_and_exit
	fi
fi

###############################################################################
## 4. COMMAND
###############################################################################

exit_code=0

case ${COMMAND} in

"deploy")
	check_docker_permissons

	DOCKER_SWARM_DEPLOY_ARGS=("docker" "stack" "deploy")

	# Include the root compose file first.
	DOCKER_SWARM_DEPLOY_ARGS+=("--compose-file" "${HIAC_CLOUD_DIR_ABS}/docker-cloud.yml")

	# Process each service. A lot of environment variables will be
	# exported during the execution of the following function.
	for_each_service deploy_service

	# This variable must be made available to the deploy command.
	export HOST_CONFIG_DIR="${container_config_dir}"

	# Define the name of the stack.
	DOCKER_SWARM_DEPLOY_ARGS+=("hiac")

	# Deploy stack
	"${DOCKER_SWARM_DEPLOY_ARGS[@]}"
	;;

"warranty")
	print_warranty
	;;

"conditions")
	print_conditions
	;;

*)
	echo "Unsupported command '${COMMAND}'." 1>&2
	print_usage_and_exit
	;;
esac

exit "${exit_code}"

